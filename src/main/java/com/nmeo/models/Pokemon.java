package com.nmeo.models;

import java.util.List;

public class Pokemon {
    // ATTRIBUTS
    public String pokemonName;
    public PokemonType type;
    public int lifePoints;
    public List<Power> powers;

    public String getPokemonName() {
        return this.pokemonName;
    }

    public PokemonType getType() {
        return this.type;
    }

    public int getLifePoints() {
        return this.lifePoints;
    }

    public List<Power> getPowers() {
        return this.powers;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public void setType(PokemonType type) {
        this.type = type;
    }
}
