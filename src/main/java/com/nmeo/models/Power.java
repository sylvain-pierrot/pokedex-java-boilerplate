package com.nmeo.models;

public class Power {
    // ATTRIBUTS
    public String powerName;
    public PokemonType damageType;
    public int damage;

    public String getPowerName() {
        return powerName;
    }
}
