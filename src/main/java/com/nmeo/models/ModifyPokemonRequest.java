package com.nmeo.models;

import java.util.List;

public class ModifyPokemonRequest {
    private String name;
    private Modifications modifications;

    public String getName() {
        return name;
    }

    public Modifications getModifications() {
        return modifications;
    }

    public static class Modifications {
        private int lifePoints;
        private List<Power> powers;

        public int getLifePoints() {
            return lifePoints;
        }

        public List<Power> getPowers() {
            return powers;
        }
    }
}
