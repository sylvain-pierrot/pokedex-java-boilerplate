package com.nmeo;

import io.javalin.Javalin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ValidationException;
import java.util.Optional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import com.nmeo.models.ListPokemon;
import com.nmeo.models.ModifyPokemonRequest;
import com.nmeo.models.Pokemon;
import com.nmeo.models.PokemonType;
import com.nmeo.models.Power;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());

    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null ? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        // ListPokemon
        ListPokemon l = new ListPokemon();

        Javalin.create()
                .get("/api/status", ctx -> {
                    logger.debug("Status handler triggered", ctx);
                    ctx.status(200);
                })
                .post("/api/create", ctx -> {
                    try {
                        Pokemon pokemon = ctx.bodyValidator(Pokemon.class)
                                .check(p -> p.getPokemonName() != null && !p.getPokemonName().isEmpty(),
                                        "Pokemon name is required")
                                .check(p -> p.getType() != null, "Pokemon type is required")
                                .check(p -> p.getLifePoints() >= 0, "Life points must be non-negative")
                                .get();

                        // Check if the Pokemon already exists in the list
                        if (l.pokemonList.stream().anyMatch(p -> p.getPokemonName().equals(pokemon.getPokemonName()))) {
                            ctx.status(400); // 400 Bad Request
                            ctx.result("Pokemon already exists");
                        } else {
                            l.pokemonList.add(pokemon);
                            logger.info("Added Pokemon: " + pokemon.getPokemonName());

                            ctx.status(200); // 200 OK
                            ctx.result("Pokemon created: " + pokemon.getPokemonName());
                        }
                    } catch (ValidationException e) {
                        logger.error("Error creating Pokemon: " + e.getMessage());
                        ctx.status(400); // 400 Bad Request
                        ctx.result(e.getMessage());
                    } catch (Exception e) {
                        logger.error("Error creating Pokemon: " + e.getMessage());
                        ctx.status(400); // 500 Internal Server Error
                        ctx.result("An error occurred while creating the Pokemon");
                    }
                })
                .get("/api/searchByName", ctx -> {
                    try {
                        String nameToSearch = ctx.queryParam("name");
                        if (nameToSearch == null || nameToSearch.isEmpty() || !(nameToSearch instanceof String)) {
                            ctx.status(400); // 400 Bad Request
                            ctx.result("Invalid name parameter");
                            return;
                        }

                        List<Pokemon> matchingPokemons = l.pokemonList.stream()
                                .filter(p -> p.getPokemonName().contains(nameToSearch))
                                .collect(Collectors.toList());

                        // Create a response map with a "result" field
                        Map<String, List<Pokemon>> response = new HashMap<>();
                        response.put("result", matchingPokemons);

                        ctx.status(200); // 200 OK
                        ctx.json(response);
                    } catch (Exception e) {
                        logger.error("Error searching Pokemon by name: " + e.getMessage());
                        ctx.status(400); // 500 Internal Server Error
                        ctx.result("An error occurred while searching for Pokemon by name");
                    }
                })
                .post("/api/modify", ctx -> {
                    try {
                        ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);

                        // Check if the request contains a valid name and modifications
                        if (request.getName() == null || request.getName().isEmpty()
                                || request.getModifications() == null) {
                            ctx.status(400); // 400 Bad Request
                            ctx.result("Invalid request parameters");
                            return;
                        }

                        // Find the Pokemon to modify
                        Optional<Pokemon> optionalPokemon = l.pokemonList.stream()
                                .filter(p -> p.getPokemonName().equals(request.getName()))
                                .findFirst();

                        if (!optionalPokemon.isPresent()) {
                            ctx.status(404); // 404 Not Found
                            ctx.result("Pokemon not found");
                            return;
                        }

                        Pokemon pokemon = optionalPokemon.get();

                        // Apply modifications to the Pokemon
                        if (request.getModifications().getLifePoints() >= 0) {
                            pokemon.setLifePoints(request.getModifications().getLifePoints());
                        }

                        if (request.getModifications().getPowers() != null) {
                            // Add new powers if they don't already exist
                            for (Power newPower : request.getModifications().getPowers()) {
                                if (pokemon.getPowers().stream()
                                        .noneMatch(p -> p.getPowerName().equals(newPower.getPowerName()))) {
                                    pokemon.getPowers().add(newPower);
                                }
                            }
                        }

                        ctx.status(200); // 200 OK
                        ctx.result("Pokemon modified successfully");
                    } catch (Exception e) {
                        logger.error("Error modifying Pokemon: " + e.getMessage());
                        ctx.status(400); // 500 Internal Server Error
                        ctx.result("An error occurred while modifying the Pokemon");
                    }
                }).get("/api/searchByType", ctx -> {
                    try {
                        String typeToSearch = ctx.queryParam("type");

                        if (typeToSearch == null || typeToSearch.isEmpty()) {
                            ctx.status(400); // 400 Bad Request
                            ctx.result("Invalid type parameter");
                            return;
                        }

                        // Convert the type to uppercase and retrieve the PokemonType enum value
                        PokemonType parsedType = PokemonType.valueOf(typeToSearch.toUpperCase());

                        List<Pokemon> matchingPokemons = l.pokemonList.stream()
                                .filter(p -> p.getType().equals(parsedType))
                                .collect(Collectors.toList());

                        // Create a response map with a "result" field
                        Map<String, List<Pokemon>> response = new HashMap<>();
                        response.put("result", matchingPokemons);

                        ctx.status(200); // 200 OK
                        ctx.json(response);
                    } catch (IllegalArgumentException ex) {
                        ctx.status(400); // 400 Bad Request
                        ctx.result("Invalid type parameter");
                    } catch (Exception e) {
                        logger.error("Error searching Pokemon by type: " + e.getMessage());
                        ctx.status(400); // 500 Internal Server Error
                        ctx.result("An error occurred while searching for Pokemon by type");
                    }
                }).start(port);
    }
}